<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/nous-contacter", name="contact")
     */
    public function index(Request $request, \Swift_Mailer $mailer): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->addFlash('notice','merci de nous avoir contacter notre equipe va vous repondre dans les plus bref delai');
//            $mail=new Mail();
//            $mail->send('felixmalchair@gmail.com','Furnitures Eshop','vous');
            $contact = $form->getData();
            $message=(new \Swift_Message('Nouveau Contact'))
            ->setFrom($contact['email'])
            ->setTo('felixmalchair@gmail.com')
             ->setBody(
                 $this->renderView(
                     'email/contact.html.twig', compact('contact')
                 ),
                 'text.html'
             )
            ;

            $mailer->send($message);
            $this->addFlash('message','merci de nous avoir contacter notre equipe va vous repondre dans les plus bref delai');
            return $this->redirectToRoute('home');
        }

        return $this->render('contact/index.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
