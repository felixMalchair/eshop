<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, [
                'label' => 'quel nom souhaitez-vous donner a votre adresse ? (domicile,seconde residence...)',
                'attr' => [
                    'placeholder' => 'Nommez votre adresse'
                ]
            ])
            ->add('firstName',TextType::class, [
                'label' => 'votre prenom pour la livraison',
                'attr' => [
                    'placeholder' => 'entrer votre prenom'
                ]
            ])
            ->add('lastName',TextType::class, [
                'label' => 'votre prenom pour la livraison',
                'attr' => [
                    'placeholder' => 'votre nom'
                ]
            ])
            ->add('company',TextType::class, [
                'label' => 'votre entreprise (falcultatif)',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Nommez votre entreprise'
                ]
            ])
            ->add('address',TextType::class, [
                'label' => 'nom de rue et numero',
                'attr' => [
                    'placeholder' => "8 rues des chats"
                ]
            ])
            ->add('postal',TextType::class, [
                'label' => 'votre code postal',
                'attr' => [
                    'placeholder' => 'entrer votre code postal'
                ]
            ])
            ->add('city',TextType::class, [
                'label' => 'votre ville',
                'attr' => [
                    'placeholder' => 'entrer votre ville'
                ]
            ])
            ->add('country',CountryType::class, [
                'label' => 'votre pays',
                'attr' => [
                    'placeholder' => 'entrez votre pays'
                ]
            ])
            ->add('phone',TelType::class, [
                'label' => 'telephone',
                'attr' => [
                    'placeholder' => 'votre telephone pour le livreur'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Validez",
                'attr' => [
                    'class' => 'btn btn-block btn-primary'
    ]
            ]);;
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
