<?php


namespace App\Dto;


use App\Entity\Category;

class SearchProduct
{
    /**
     * @var string
     */
    public $string = '';

    /**
     * @var Category[]
     */
    public $categories = [];
}